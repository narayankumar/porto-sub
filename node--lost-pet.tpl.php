<?php 

if ($items = field_get_items('node', $node, 'field_image')) {
  if (count($items) == 1) {
    $image_slide = 'false';
  }
  elseif (count($items) > 1) {
    $image_slide = 'true';
  }
}  
  


?>

<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>  post post-large blog-single-post"<?php print $attributes; ?>>
	<div class="experttalk">
	  	<h4>Lost <strong>Pet</strong></h4>
	  </div>

  <?php if (render($content['field_image'])) : ?> 
	  
	  <?php if ($image_slide == 'true'): ?>
		  <div class="post-image">
			  <div class="owl-carousel" data-plugin-options='{"items":1}'>
					  <?php if (render($content['field_image'])) : ?>
					    <?php print render($content['field_image']); ?>
					  <?php endif; ?>
			    </div>    
			</div>
		<?php endif; ?>
			
		<?php if ($image_slide == 'false'): ?>
		  <div class="single-post-image post-image">
		    <?php print render($content['field_image']); ?>
		  </div>  
		<?php endif; ?>
			
  <?php endif; ?>
	
	<div class="post-content">

	  <?php print render($title_prefix); ?>
	    <h2 <?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
	  <?php print render($title_suffix); ?>
	    
	  <?php if ($display_submitted): ?>
	  
	    <div class="post-meta">
				<span class="post-meta-user"><i class="fa fa-user"></i> <?php print t('Contributed by ') . $name; ?></span>
				<?php if (render($content['field_pet_city'])): ?> 
				  <span class="post-meta-building"><i class="fa fa-building-o"></i></span><span><?php print render($content['field_pet_city']); ?> </span>
				<?php endif; ?> 
				<span class="post-meta-comments"><i class="fa fa-comments"></i> <a href="<?php print $node_url;?>/#comments"><?php print $comment_count; ?> <?php print t('Comment'); ?><?php if ($comment_count != "1" ) { echo "s"; } ?></a></span>
				<?php if (!empty($content['field_lost_pet_found'])): ?> 
				  <span class="post-meta-building red"><i class="fa fa-thumbs-o-up"></i></span><span class="red"><?php print render($content['field_lost_pet_found']); ?> </span>
				<?php endif; ?>
			</div>
		
	  <?php endif; ?>
	   
	  <div class="article_content"<?php print $content_attributes; ?>>
		<div class="lost-pet-name-breed">
		<span class="post-meta-building">
			<?php print render($content['field_lost_pet_name']); ?>
		</span>
		<span class="post-meta-building">
			<?php print render($content['field_lost_pet_breed']); ?>
		</span>
		</div>
		<div class="lost-pet-body"><?php print render($content['body']); ?></div>
		<div class="lost-pet-body"><?php print render($content['field_lost_pet_identity_marks']); ?></div>
		<div class="lost-pet-body"><?php print render($content['field_lost_pet_contact_info']); ?></div>
		
	    <?php
	      // Hide comments, tags, and links now so that we can render them later.
	      hide($content['comments']);
	      hide($content['links']);
	      print render($content);    
	    ?>
	  </div> 
  
	</div>
	
	<?php
    // Remove the "Add new comment" link on the teaser page or if the comment
    // form is being displayed on the same page.
    if ($teaser || !empty($content['comments']['comment_form'])) {
      unset($content['links']['comment']['#links']['comment-add']);
    }
    // Only display the wrapper div if there are links.
    $links = render($content['links']);
    if ($links):
  ?>
    <?php if (!$teaser): ?>
	    <div class="link-wrapper">
	      <?php print $links; ?>
	    </div>
	  <?php endif; ?>  
  <?php endif; ?>
  
  <?php print render($content['comments']); ?>

</article>
<!-- /node -->