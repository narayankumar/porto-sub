<?php
/*
 * Prefix your custom functions with porto_sub. For example:
 * porto_sub_form_alter(&$form, &$form_state, $form_id) { ... }
 */

/**
 * Overrides theme_item_list().
 */
function porto_sub_item_list($variables) {
  $items = $variables['items'];
  $title = $variables['title'];
  $type = $variables['type'];
  $variables['attributes']['class'] = 'pagination pagination-sm pull-left';
  $attributes = $variables['attributes'];

  // Only output the list container and title, if there are any list items.
  // Check to see whether the block title exists before adding a header.
  // Empty headers are not semantic and present accessibility challenges.
  $output = '';
  if (isset($title) && $title !== '') {
    $output .= '<h3>' . $title . '</h3>';
  }

  if (!empty($items)) {
    $output .= "<$type" . drupal_attributes($attributes) . '>';
    $num_items = count($items);
    $i = 0;
    foreach ($items as $item) {
      $attributes = array();
      $children = array();
      $data = '';
      $i++;

      if ( is_array($item) && in_array('pager-current', $item['class'])) {
        $item['class'] = array('active');
        $item['data'] = '<a href="#">' . $item['data'] . '</a>';
      }

      if (is_array($item)) {
        foreach ($item as $key => $value) {
          if ($key == 'data') {
            $data = $value;
          }
          elseif ($key == 'children') {
            $children = $value;
          }
          else {
            $attributes[$key] = $value;
          }
        }
      }
      else {
        $data = $item;
      }
      if (count($children) > 0) {
        // Render nested list.
        $data .= theme_item_list(array('items' => $children, 'title' => NULL, 'type' => $type, 'attributes' => $attributes));
      }

      $output .= '<li' . drupal_attributes($attributes) . '>' . $data . "</li>\n";
    }
    $output .= "</$type>";
  }

  return $output;

}

/**
 * Impelements hook_form_alter()
 */
function porto_sub_form_alter(&$form, &$form_state, $form_id) { 

  if ($form_id == 'search_block_form') {
    
    unset($form['search_block_form']['#title']); // Change the text on the label element
    unset($form['search_block_form']['#title_display']); // Toggle label visibilty
    $form['search_block_form']['#size'] = 40;  // define size of the textfield
    $form['search_block_form']['#default_value'] = t('Search...'); // Set a default value for the textfield
    
    // Add extra attributes to the text box
    $form['search_block_form']['#attributes']['class'] = array('form-control', 'search');
    // Add extra attributes to the text box
       
    $form['actions']['submit'] =  array(
      '#type' => 'submit',
    	'#prefix' => '<span class="input-group-btn"><button class="btn btn-default" type="submit"><i class="fa fa-search">',
    	'#suffix' => '</i></button></span>',	
    );  
  }
}


function porto_sub_theme($existing, $type, $theme, $path) {
  return array(
    'comment_form' => array(
      'render element' => 'form',
    ),
  );
}


function porto_sub_comment_form($variables) {
  //kpr($variables);
  $author = drupal_render($variables['form']['author']);
  $subject = drupal_render($variables['form']['subject']);
  hide ($variables['form']['comment_body']['und']['0']['format']);
  hide ($variables['form']['subject']);
  $everything_else = drupal_render_children($variables['form']);
  return $author . $subject . $everything_else;
}


