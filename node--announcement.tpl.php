<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>  post post-large blog-single-post"<?php print $attributes; ?>>
	<div class="experttalk">
	  	<h4><strong>Announcement</strong></h4>
	</div>

    <div class="single-post-image post-image">
	    <?php print render($content['field_announcement_image']); ?>
	</div>
	
	<div class="post-content">
	  <?php print render($title_prefix); ?>
	    <h2 <?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
	  <?php print render($title_suffix); ?>
	   
	  <div class="article_content"<?php print $content_attributes; ?>>

	    <?php
	      // Hide comments, tags, and links now so that we can render them later.
	      hide($content['comments']);
	      hide($content['links']);
	      hide($content['field_announcement_link']);
	      print render($content);
	    ?>
	  </div>
	
	  <div class="announcement"><?php print render($content['field_announcement_link']); ?></div>

</article>
<!-- /node -->