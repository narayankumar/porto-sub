<?php 
if ($items = field_get_items('node', $node, 'field_image')) {
  if (count($items) == 1) {
    $image_slide = 'false';
  }
  elseif (count($items) > 1) {
    $image_slide = 'true';
  }
}

$uid2 = $uid;
$uid = user_load($node->uid);

if (module_exists('profile2')) {  
  $profile = profile2_load_by_user($uid, 'main');
}

?>

<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>  post post-large blog-single-post"<?php print $attributes; ?>>

	    <div class="experttalk">
	      <h4>Directory <strong>Listing</strong></h4>
	    </div>
	    <div class="directory-logo">
	      <?php print render($content['field_logo']); ?>
	    </div>  
	
	<div class="post-content post-content-directory">

	  <?php print render($title_prefix); ?>
	    <h2 <?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
	  <?php print render($title_suffix); ?>
		
	    <div class="post-meta">
		  
		  	<?php if (render($content['field_directory_category'])): ?>
			    <div class="post-meta-tag"><?php print render($content['field_directory_category']); ?></div>
			<?php endif; ?>
			
			<?php if ($uid2 !== "3"): ?>
		      <span class="post-meta-user"><i class="fa fa-user"></i> <?php print t('Business or Service Owner: ') . $name; ?></span>
		    <?php endif; ?>
		
		  <span class="post-meta-comments"><i class="fa fa-comments"></i> <a href="<?php print $node_url;?>/#comments"><?php print $comment_count; ?> <?php print t('Comment'); ?><?php if ($comment_count != "1" ) { echo "s"; } ?></a></span>	
	    </div>
	    
	    <?php if (render($content['field_directory_rating'])): ?>
	    <div class="star-rating">
		  <?php print render($content['field_directory_rating']); ?>
		</div>
		<?php endif; ?>

      	<?php if (render($content['field_directory_phone'])): ?>
		  <span class="post-meta-phone"><i class="fa fa-phone"></i></span>
		  <div class="directory-phone">
		    <?php print render($content['field_directory_phone']); ?>
		  </div>
		<?php endif; ?>
		
		<?php if (render($content['field_directory_url_link'])): ?>
		  <span class="post-meta-globe"><i class="fa fa-globe"></i></span>
		  <div class="directory-globe">
		    <?php print render($content['field_directory_url_link']); ?>
		  </div>
		<?php endif; ?>
		
		<?php if (render($content['field_directory_email'])): ?>
		  <span class="post-meta-envelope"><i class="fa fa-envelope"></i></span>
		  <div class="directory-email">
		    <?php print render($content['field_directory_email']); ?>
		  </div>
		<?php endif; ?>
		
		<?php if (render($content['field_directory_download'])): ?>
		  <span class="post-meta-download"><i class="fa fa-download"></i></span>
		  <div class="directory-download">
		    <?php print render($content['field_directory_download']); ?>
		  </div>
		<?php endif; ?>
		<?php if (render($content['field_directory_address'])): ?>
		  <span class="post-meta-address"><i class="fa fa-home"></i></span>
		  <div class="directory-address">
		    <?php print render($content['field_directory_address']); ?>
		  </div>
		<?php endif; ?>
	   
	  <div class="article_content"<?php print $content_attributes; ?>>
		
		<div class="post-meta-briefcase"><i class="fa fa-briefcase"></i> <?php print render($content['field_directory_description']); ?></div>
	    
	    <?php
	      // Hide comments, tags, and links now so that we can render them later.
	      hide($content['comments']);
	      hide($content['links']);
          hide($content['field_image']);
	      hide($content['field_share_product_service']);
	      print render($content);
	    ?>
	  </div>
	  
		<?php if (!$page && $teaser): ?>
	  
	      <div class="post-meta">
		    <a href="<?php print $node_url; ?>" class="btn btn-mini btn-primary pull-right"><?php echo t('Read more...'); ?></a>
		  </div>

	    <?php endif; ?> 
  
	</div>
	
	<?php
    // Remove the "Add new comment" link on the teaser page or if the comment
    // form is being displayed on the same page.    

    if ($teaser || !empty($content['comments']['comment_form'])) {
      unset($content['links']['comment']['#links']['comment-add']);
    }
    // Only display the wrapper div if there are links.
    $links = render($content['links']);
    if ($links):
  ?>
    <?php if (!$teaser): ?>
	  <div class="single-post-image post-image">
		<?php print render($content['field_image']); ?>
	  </div>
	
	  <div class="directory-bottom">
	    <span class="directory-sharing"><?php print render($content['field_share_product_service']); ?></span>
	  </div>
	    <div class="link-wrapper">
	      <?php print $links; ?>
	    </div>
	  <?php endif; ?>  
  <?php endif; ?>
  
  <?php print render($content['comments']); ?>

</article>
<!-- /node -->