build2014092701
- .region-sidebar-right span.views-field-field-guest-name {padding:0 0}
- .region-sidebar-right img {margin:0 0}
- #footer,
- #footer .footer-copyright {margin-top:0px;padding:0 0;}
- node--news.tpl.php changed - pet news changed to ginger news
- ginger news changed in node label
- node--article.tpl.php changed - pet articles changed to ginger digest
- ginger digest changed in node label
- node--opinion.tpl.php changed - your opinion changed to parent talk
- parent talk changed in node label 

build2014092102
- article-row-front margin-right reduced to 5px

build2014092101a
- css lines to put a border-right on front page blocks

build2014092101
- div#block-views-recent-articles-block-block-9 {clear:left}
- .view-recent-articles-block .views-row {margin-left:5px}
- remove css line:
  {clear:left;float:left;position:relative;padding:5px;border:1px solid #dedede}
  which is under -recent-articles-block-block-8
- div#block-views-recent-articles-block-block-8 .views-row {border-right: 1px solid #ccc}
- div#block-views-recent-articles-block-block-8 .views-row-last {border-right: none}
- form#user-login .pagination>li>a {border:none}
- .view-lost-pet-teasers-page .views-field-field-lost-pet-found,
- .view-found-pet-teasers-page .views-field-field-found-pet-reunited,
- .view-adoption-teasers-page .views-field-field-adopted
  {margin: -40px 0 15px 2px;}
- div.row.col-md-7.post-meta-comments {margin-left:42%}
- div.row.col-md-9.post-meta-comments {margin-left:17%}

build2014091901
- login msg for anon users on contest pages
- template file edits with new code
- new css to go with above: .login-line, .contest-closed {margin-bottom:25px}

build2014091802a
- custom.css changed body to 13px from 12
- change 'search by rating' fields to small caps
-.field-name-field-directory-geofield - remove 180px margin-top
- directory-bottom - clear left to make social icons start on fresh line
- change to node--directory-entry.tpl.php to shift images under map

build2014091801
- change adoption, lost pet, found pet templates to show red instead of green markup
- body {font-size: 13px;line-height: 20px;}
- .body .form-text {height:24px}
- .views-exposed-form .views-exposed-widget .form-submit {margin-top:2.1em}
- div.view-filters {margin-bottom: 25px;padding-bottom:10px;border-bottom: 1px solid #dedede}
- .page-voices ul.action-links // replace the guestcol line with this
- .page-pet-photos ul.action-links // replace gallery line with this
- .page-pet-adoption ul.action-links // replace adoption lines with this (2 lines)
- delete: .form-managed-file .form-file {margin:10px}
- change the following lines from the 'galleries' line:
- .page-pet-photos #album {width:80%; padding: 0px 0px;margin-top:33px}
- .page-pet-photos div.container {padding: 0px 0px}
- div.user-album {display:none} //hide 'user album' title
- .views-exposed-widget label {
font-size: 11px;font-style: normal;
line-height: 20px;
margin-right: 3px;
text-transform: uppercase;
font-weight: 700;
}

build2014091602b
- added css margin of 50px for category field in opinion and opinion-invite node

build2014091602a
- opinion template to show category on node
- opinion-invite template to show category on node
- news template edited to show category

build2014091602
- opinion category field css
- opinion invite category field css
- front page blocks in right sidebar bottom margins
- .category added to style category field

build2014091502 

- theming login modal window
- re-united link in red in teasers page and node page
- pet adoption and pet videos teasers page - right-most element aligned properly
- message on node-edit forms moved from top header block to above content block
- increased vertical spacing between upload and submit buttons
