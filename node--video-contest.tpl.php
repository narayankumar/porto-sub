
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>  post post-large blog-single-post"<?php print $attributes; ?>>
  <div class="experttalk">
    <h4>Video <strong>Contest</strong></h4>
  </div>

  <?php if (render($content['field_contest_image'])) : ?> 
    <div class="single-post-image post-image">
	  <?php print render($content['field_contest_image']); ?>
    </div>		
  <?php endif; ?>
	
  <div class="post-content">

	<?php print render($title_prefix); ?>
	  <h2 <?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
	<?php print render($title_suffix); ?>
	   
	<div class="article_content"<?php print $content_attributes; ?>>
	  <div class="post-meta">
		<span class="post-meta-building">
		  <?php print render($content['field_contest_last_date']); ?>
		</span>
		<span class="post-meta-building">
		  <?php print render($content['field_contest_closed']); ?>
		</span>
	  </div>
	  <div><?php print render($content['body']); ?></div>
		
	    
		<?php if (($variables['field_contest_closed'][0]['value'] == 0) && (user_is_logged_in())): ?>
		  <div class="btn btn-primary">
		    <?php print render($content['field_enter_contest']); ?>
		  </div>
		 <?php elseif (($variables['field_contest_closed'][0]['value'] == 0) && (user_is_anonymous())): ?>
		  <div class="login-line black">
		    <?php echo 'To enter contest, you must <a href="../../../user/login">login</a> or <a href="../../../user/register">register</a>'; ?>
		  </div>
		<?php else: ?>
		  <div class="contest-closed black">
		    <?php echo "This contest is closed"; ?>
		  </div>
		<?php endif; ?>

		
	    <?php
	      // Hide comments, tags, and links now so that we can render them later.
	      hide($content['comments']);
	      hide($content['links']);
	      hide($content['field_enter_contest']);
	      print render($content); 
	    ?>
	  </div> 
      
	</div>
	
	<?php
    // Remove the "Add new comment" link on the teaser page or if the comment
    // form is being displayed on the same page.
    if ($teaser || !empty($content['comments']['comment_form'])) {
      unset($content['links']['comment']['#links']['comment-add']);
    }
    // Only display the wrapper div if there are links.
    $links = render($content['links']);
    if ($links):
    ?>
      <?php if (!$teaser): ?>
	    <div class="link-wrapper">
	      <?php print $links; ?>
	    </div>
	  <?php endif; ?>  
    <?php endif; ?>
  
  <?php print render($content['comments']); ?>

</article>
<!-- /node -->