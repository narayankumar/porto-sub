<?php 

if ($items = field_get_items('node', $node, 'field_image')) {
  if (count($items) == 1) {
    $image_slide = 'false';
  }
  elseif (count($items) > 1) {
    $image_slide = 'true';
  }
}  

$uid2 = render($content['field_contest_submitter']);
$uid = user_load($node->uid);

if (module_exists('profile2')) {  
  $profile = profile2_load_by_user($uid, 'main');
}

?>

<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>  post post-large blog-single-post"<?php print $attributes; ?>>
	<div class="experttalk">
	  	<h4>Parent's <strong>Column</strong></h4>
	</div>

	<?php if (render($content['field_image'])) : ?> 

		  <?php if ($image_slide == 'true'): ?>
			  <div class="post-image">
				<div class="owl-carousel" data-plugin-options='{"items":1}'>
				  <?php if (render($content['field_image'])) : ?>
				    <?php print render($content['field_image']); ?>
				  <?php endif; ?>
				</div>    
			  </div>
		  <?php endif; ?>

		  <?php if ($image_slide == 'false'): ?>
			  <div class="single-post-image post-image">
			    <?php print render($content['field_image']); ?>
			  </div>  
		  <?php endif; ?>
		
	<?php endif; ?>
	
	<div class="post-content">
	  <?php print render($title_prefix); ?>
	    <h2 <?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
	  <?php print render($title_suffix); ?>
	  
	    <div class="post-meta">
		    <span class="post-meta-user"><i class="fa fa-user"></i>Contributed by </span>
		    <span><?php print render($content['field_contest_submitter']); ?></span>
		    <span class="post-meta-user"><i class="fa fa-quote-right"></i>Subject: </span>
		    <span><?php print render($content['field_opinion_invite_ref']); ?></span>
			<span class="post-meta-comments"><i class="fa fa-comments"></i> <a href="<?php print $node_url;?>/#comments"><?php print $comment_count; ?> <?php print t('Comment'); ?><?php if ($comment_count != "1" ) { echo "s"; } ?></a></span>
		</div>
          <div class="category"><?php print render($content['field_voices_category']); ?></div>	   
	  <div class="article_content"<?php print $content_attributes; ?>>
		
	    <?php
	      // Hide comments, tags, and links now so that we can render them later.
	      hide($content['comments']);
	      hide($content['links']);
	      hide($content['field_contest_submitter']);
	      print render($content);
	    ?>
	  </div>
	
	<?php
    // Remove the "Add new comment" link on the teaser page or if the comment
    // form is being displayed on the same page.
    if ($teaser || !empty($content['comments']['comment_form'])) {
      unset($content['links']['comment']['#links']['comment-add']);
    }
    // Only display the wrapper div if there are links.
    $links = render($content['links']);
    if ($links):
  ?>
    <?php if (!$teaser): ?>
	    <div class="link-wrapper">
	      <?php print $links; ?>
	    </div>
	  <?php endif; ?>  
  <?php endif; ?>
  
  <?php print render($content['comments']); ?>

</article>
<!-- /node -->
