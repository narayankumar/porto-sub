<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>  post post-large blog-single-post"<?php print $attributes; ?>>
	<div class="experttalk">
	  	<h4>Invite <strong>Opinion</strong></h4>
	</div>

    <div class="single-post-image post-image">
	    <?php print render($content['field_image']); ?>
	</div>
	
	<div class="post-content">
	  <?php print render($title_prefix); ?>
	    <h2 <?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
	  <?php print render($title_suffix); ?>
	  
	    <div class="post-meta">
		    <span class="post-meta-user"><i class="fa fa-cog"></i></span>
		    <span><?php print render($content['field_opinion_invite_status']); ?></span>
		    
		    <?php if(!empty($variables['field_link_to_content'][0]['url'])): ?><span><i class="fa fa-reply"></i> </span><span><?php print render($content['field_link_to_content']); ?><?php endif; ?></span>
		    
		</div>
          <div class="category"><?php print render($content['field_voices_category']); ?></div>	   
	  <div class="article_content"<?php print $content_attributes; ?>>
		
		<p><?php print render($content['body']); ?></p>
		
		<?php if ($variables['field_opinion_invite_status'][0]['value'] == 0): ?>
		  <div class="btn btn-primary"><?php print render($content['field_send_opinion']); ?></div>
		<?php endif; ?>
		<div class="after-main-content">
		  <?php print render($content); ?>
		</div>
		
	  </div>

</article>
<!-- /node -->
