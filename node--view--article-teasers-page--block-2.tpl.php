<li>
  <div class="post-image">
	 <?php if (render($content['field_image'])) :?>
	  <div class="img-thumbnail">
	    <a href="<?php print $node_url; ?>">
	      <?php if (render($content['field_image'])): ?>  
	        <img src="<?php echo file_create_url($node->field_image['und'][0]['uri']); ?>" width="30" height="30" alt="">
	      <?php endif; ?>
	    </a>
	  </div>
	  <?php endif; ?>
	
	<?php if (render($content['field_guestcol_image'])) :?>
	  <div class="img-thumbnail">
	    <a href="<?php print $node_url; ?>">
	      <?php if (render($content['field_guestcol_image'])): ?>  
	        <img src="<?php echo file_create_url($node->field_guestcol_image['und'][0]['uri']); ?>" width="30" height="30" alt="">
	      <?php endif; ?>
	    </a>
	  </div>
	  <?php endif; ?>
	
  </div>
  <div class="post-info">
    <a href="<?php print $node_url; ?>" class="tabbed-title"><?php echo $title; ?></a>
    <div class="post-body"><?php if (render($content['body'])) :?><?php print render($content['body']); ?><?php endif; ?></div>
  </div>    
</li>  