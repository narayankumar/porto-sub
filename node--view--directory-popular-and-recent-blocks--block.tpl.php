<li>
  <div class="post-image">
	 <?php if (render($content['field_image'])) :?>
	  <div class="img-thumbnail img-thumbnail-popular">
	    <a href="<?php print $node_url; ?>">
	      <?php if (render($content['field_image'])): ?>  
	        <img src="<?php echo file_create_url($node->field_image['und'][0]['uri']); ?>" alt="">
	      <?php endif; ?>
	    </a>
	  </div>
	  <?php endif; ?>
  </div>
  <div class="post-info">
    <a href="<?php print $node_url; ?>" class="tabbed-title"><?php echo $title; ?></a>
  
  </div>    
</li>  