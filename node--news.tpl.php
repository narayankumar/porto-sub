<?php 

if ($items = field_get_items('node', $node, 'field_news_image')) {
  if (count($items) == 1) {
    $image_slide = 'false';
  }
  elseif (count($items) > 1) {
    $image_slide = 'true';
  }
}  
$uid2 = render($content['field_contributed_by']);
$uid = user_load($node->uid);

if (module_exists('profile2')) {  
  $profile = profile2_load_by_user($uid, 'main');
}

?>

<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>  post post-large blog-single-post"<?php print $attributes; ?>>
	<div class="experttalk">
	  	<h4>Ginger <strong>News</strong></h4>
	  </div>

  <?php if (render($content['field_news_image'])) : ?> 
	  
	  <?php if ($image_slide == 'true'): ?>
		  <div class="post-image">
			  <div class="owl-carousel" data-plugin-options='{"items":1}'>
					  <?php if (render($content['field_news_image'])) : ?>
					    <?php print render($content['field_news_image']); ?>
					  <?php endif; ?>
			    </div>    
			</div>
		<?php endif; ?>
			
		<?php if ($image_slide == 'false'): ?>
		  <div class="single-post-image post-image">
		    <?php print render($content['field_news_image']); ?>
		  </div>  
		<?php endif; ?>
			
  <?php endif; ?>	
	
	<div class="post-content">

	  <?php print render($title_prefix); ?>
	    <h2 <?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
	  <?php print render($title_suffix); ?>
	  
	    <div class="post-meta">
		  <?php if (!empty($uid2)): ?>
				<span class="post-meta-user"><i class="fa fa-user"></i>Contributed by</span>
				<span><?php print render($content['field_contributed_by']); ?></span>
		  <?php endif; ?>
				<?php if (render($content['field_tags'])): ?> 
				  <span class="post-meta-tag"><i class="fa fa-tag"></i> <?php print render($content['field_tags']); ?> </span>
				<?php endif; ?> 
				<span class="post-meta-comments"><i class="fa fa-comments"></i> <a href="<?php print $node_url;?>/#comments"><?php print $comment_count; ?> <?php print t('Comment'); ?><?php if ($comment_count != "1" ) { echo "s"; } ?></a></span>
			</div>
	   
	  <div class="article_content"<?php print $content_attributes; ?>>
	    <?php
	      // Hide comments, tags, and links now so that we can render them later.
	      hide($content['comments']);
	      hide($content['links']);
	      hide($content['field_tags']);
	      hide($content['field_news_image']);
	      print render($content);
	    ?>
	  </div>  
  
	</div>
	
	<?php
    // Remove the "Add new comment" link on the teaser page or if the comment
    // form is being displayed on the same page.
    if ($teaser || !empty($content['comments']['comment_form'])) {
      unset($content['links']['comment']['#links']['comment-add']);
    }
    // Only display the wrapper div if there are links.
    $links = render($content['links']);
    if ($links):
  ?>
    <?php if (!$teaser): ?>
	    <div class="link-wrapper">
	      <?php print $links; ?>
	    </div>
	  <?php endif; ?>  
  <?php endif; ?>
  
  <?php print render($content['comments']); ?>

</article>
<!-- /node -->