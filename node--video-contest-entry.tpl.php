<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>  post post-large blog-single-post"<?php print $attributes; ?>>
  <div class="experttalk">
    <h4>Contest <strong>Entry</strong></h4>
  </div>

  <div class="single-post-image post-image">
	<?php print render($content['field_video_contest_embed']); ?>
  </div>

  <div class="post-content">
	<?php print render($title_prefix); ?>
	<h2 <?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
	<?php print render($title_suffix); ?>
	  
	<div class="post-meta">
	  <span class="post-meta-user"><i class="fa fa-user"></i>Submitted by </span>
	  <span class="post-meta-user"><?php print render($content['field_contest_submitter']); ?></span>
	  <span class="post-meta-comments"><i class="fa fa-comments"></i> <a href="<?php print $node_url;?>/#comments"><?php print $comment_count; ?> <?php print t('Comment'); ?><?php if ($comment_count != "1" ) { echo "s"; } ?></a></span>
	</div>
	   
	<div class="article_content"<?php print $content_attributes; ?>>
      <div class="contest-ref"><?php print render($content['field_video_contest_reference']); ?></div>
	    <?php
	    // Hide comments, tags, and links now so that we can render them later.
	    hide($content['comments']);
	    hide($content['links']);
	    print render($content);
	    ?>
	  </div>
	
	  <?php
        // Remove the "Add new comment" link on the teaser page or if the comment
        // form is being displayed on the same page.
        if ($teaser || !empty($content['comments']['comment_form'])) {
        unset($content['links']['comment']['#links']['comment-add']);
      }
      // Only display the wrapper div if there are links.
      $links = render($content['links']);
      if ($links):
      ?>
        <?php if (!$teaser): ?>
	      <div class="link-wrapper">
	    <?php print $links; ?>
	  </div>
	  <?php endif; ?>  
      <?php endif; ?>
  
  <?php print render($content['comments']); ?>

</article>
<!-- /node -->