<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> post post-large"<?php print $attributes; ?>>
	
	<?php print render($title_prefix); ?>
	  <h4<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h4>
	<?php print render($title_suffix); ?>
	
	<div class="post-meta">
			<span class="post-meta-user"><i class="fa fa-user"></i> <?php if ($uid !== '3'): ?><?php print t('Contributed by '); ?><?php print $name; ?><?php endif; ?></span>
			<span class="post-meta-comments"><i class="fa fa-comments"></i> <a href="<?php print $node_url;?>/#comments"><?php print $comment_count; ?> <?php print t('Comment'); ?><?php if ($comment_count != "1" ) { echo "s"; } ?></a></span>

	</div>

<div class="article_content"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      print render($content);
    ?>
  </div>

  <?php
    // Remove the "Add new comment" link on the teaser page or if the comment
    // form is being displayed on the same page.
    if ($teaser || !empty($content['comments']['comment_form'])) {
      unset($content['links']['comment']['#links']['comment-add']);
    }
    // Only display the wrapper div if there are links.
    $links = render($content['links']);
    if ($links):
  ?>
    <div class="album-links link-wrapper">
      <?php print $links; ?>
    </div>
  <?php endif; ?>
  <div class="album-comments">
  <?php print render($content['comments']); ?>
  </div>

</div>